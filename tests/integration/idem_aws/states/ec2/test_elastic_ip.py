import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_standard_elastic_ip(hub, ctx):
    # creating elastic ip with standard scope is not supported in the aws account used for testing. Hence this test
    # is skipped for testing in real aws.
    if not hub.tool.utils.is_running_localstack(ctx):
        return
    # Create elastic ip for standard domain
    elastic_ip_temp_name = "idem-test-elastic-ip-" + str(uuid.uuid4())

    # Create elastic_ip with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.elastic_ip.present(
        test_ctx, name=elastic_ip_temp_name, domain="standard"
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert elastic_ip_temp_name == ret.get("new_state").get("name")
    assert "standard" == ret.get("new_state").get("domain")
    assert "allocation_id_known_after_present" == ret.get("new_state").get(
        "allocation_id"
    )
    assert f"Would create aws.ec2.elastic_ip '{elastic_ip_temp_name}'" in ret["comment"]

    # Create elastic_ip
    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx, name=elastic_ip_temp_name, domain="standard"
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret.get("new_state").get("name")
    assert ret.get("new_state").get("resource_id")
    assert "standard" == ret.get("new_state").get("domain")
    assert f"Created '{elastic_ip_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")

    # Describe elastic_ip
    describe_ret = await hub.states.aws.ec2.elastic_ip.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.elastic_ip.present"
    )
    elastic_ip_resource = describe_ret.get(resource_id).get(
        "aws.ec2.elastic_ip.present"
    )
    described_resource_map = dict(ChainMap(*elastic_ip_resource))
    assert "domain" in described_resource_map
    assert described_resource_map.get("resource_id")
    assert "standard" == described_resource_map.get("domain")
    assert described_resource_map.get("name")

    # Delete elastic_ip with test flag
    ret = await hub.states.aws.ec2.elastic_ip.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.elastic_ip {resource_id}" in ret["comment"]

    # Delete elastic_ip
    ret = await hub.states.aws.ec2.elastic_ip.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret.get("old_state").get("name")
    assert "standard" == ret.get("old_state").get("domain")
    assert ret.get("old_state").get("resource_id")
    assert f"Deleted '{resource_id}'" in ret["comment"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.elastic_ip.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.ec2.elastic_ip '{resource_id}' already absent" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_elastic_ip_with_vpc(hub, ctx):
    # Create elastic ip for vpc domain
    elastic_ip_temp_name = "idem-test-elastic-ip-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": elastic_ip_temp_name}]

    # Create elastic ip with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.elastic_ip.present(
        test_ctx, name=elastic_ip_temp_name, domain="vpc", tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret.get("new_state").get("name")
    assert "vpc" == ret.get("new_state").get("domain")
    assert ret.get("new_state").get("tags")
    assert tags == ret.get("new_state").get("tags")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, ret.get("new_state").get("tags")
    )
    assert f"Would create aws.ec2.elastic_ip '{elastic_ip_temp_name}'" in ret["comment"]

    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx, name=elastic_ip_temp_name, domain="vpc", tags=tags
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret.get("new_state").get("name")
    assert "vpc" == ret.get("new_state").get("domain")
    assert ret.get("new_state").get("resource_id")
    assert ret.get("new_state").get("tags")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, ret.get("new_state").get("tags")
    )
    assert f"Created '{elastic_ip_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")

    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")

    new_tags = [{"Key": "Name", "Value": "Updated"}]
    # Update tags with test flag
    ret = await hub.states.aws.ec2.elastic_ip.present(
        test_ctx,
        name=resource_id,
        resource_id=resource_id,
        domain="vpc",
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    new_resource = ret["new_state"]
    assert "vpc" == new_resource.get("domain")
    assert resource_id == new_resource.get("resource_id")
    assert new_resource.get("allocation_id")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, new_resource.get("tags")
    )
    # test for removal of old tags having same name and different value
    old_resource = ret["old_state"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, old_resource.get("tags")
    )

    # Update tags
    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx, name=resource_id, resource_id=resource_id, domain="vpc", tags=new_tags
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    new_resource = ret["new_state"]
    assert new_tags == new_resource.get("tags")
    # test for removal of old tags having same name and different value
    old_resource = ret["old_state"]
    assert resource_id == new_resource.get("resource_id")
    assert new_resource.get("allocation_id")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, old_resource.get("tags")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, new_resource.get("tags")
    )
    assert f"Update tags" in str(ret["comment"])

    # Describe elastic_ip
    describe_ret = await hub.states.aws.ec2.elastic_ip.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.ec2.elastic_ip.present"
    )
    resource = describe_ret.get(resource_id).get("aws.ec2.elastic_ip.present")
    described_map = dict(ChainMap(*resource))
    assert resource_id in described_map.get("name")
    assert resource_id in described_map.get("resource_id")
    assert described_map.get("allocation_id")
    assert "vpc" == described_map.get("domain")
    describe_tags = described_map.get("tags")

    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        describe_tags, new_tags
    )

    # Delete elastic_ip with test flag
    ret = await hub.states.aws.ec2.elastic_ip.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.elastic_ip {resource_id}" in ret["comment"]

    # Delete elastic_ip
    ret = await hub.states.aws.ec2.elastic_ip.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret.get("old_state").get("name")
    assert "vpc" == ret.get("old_state").get("domain")
    assert ret.get("old_state").get("resource_id")
    assert ret.get("old_state").get("tags")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        new_tags, ret.get("old_state").get("tags")
    )
    assert f"Deleted '{resource_id}'" in ret["comment"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.ec2.elastic_ip.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.ec2.elastic_ip '{resource_id}' already absent" in ret["comment"]
