import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_auto_scaling_group(hub, ctx, aws_ec2_subnet, aws_launch_configuration):
    # Init
    name = "idem-test-asg-" + str(uuid.uuid4())
    launch_config_name = aws_launch_configuration.get("resource_id")
    sub_net_id = aws_ec2_subnet.get("SubnetId")
    min_size = 3
    max_size = 5
    desired_capacity = 4
    tags = [{"key": "Name", "value": "tag-1", "propagate_at_launch": True}]

    non_existent_auto_scaling_group_name = "dummy"
    updated_min_size = 4
    updated_max_size = 6
    updated_desired_capacity = 5
    updated_tags = [{"key": "Name", "value": "tag-2", "propagate_at_launch": True}]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Test-run auto-scaling group create
    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        test_ctx,
        name=name,
        resource_id=name,
        min_size=min_size,
        max_size=max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"Would create aws.autoscaling.auto_scaling_group '{name}'",
    )
    resource = ret.get("new_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert min_size == resource["min_size"]
    assert max_size == resource["max_size"]
    assert desired_capacity == resource["desired_capacity"]
    assert tags == resource["tags"]

    # Create actual auto-scaling-group-resource
    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        ctx,
        name=name,
        resource_id=name,
        min_size=min_size,
        max_size=max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (f"Created aws.autoscaling.auto_scaling_group '{name}'",)
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert min_size == resource["min_size"]
    assert max_size == resource["max_size"]
    assert desired_capacity == resource["desired_capacity"]
    assert tags == resource["tags"]

    # Test-run auto-scaling group update with no changes
    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        test_ctx,
        name=name,
        resource_id=name,
        min_size=min_size,
        max_size=max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"aws.autoscaling.auto_scaling_group '{name}' already exists",
    )
    resource = ret.get("new_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert min_size == resource["min_size"]
    assert max_size == resource["max_size"]
    assert desired_capacity == resource["desired_capacity"]
    assert tags == resource["tags"]
    assert ret["old_state"] == ret["new_state"]

    # Update actual auto-scaling-group-resource with no changes
    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        ctx,
        name=name,
        resource_id=name,
        min_size=min_size,
        max_size=max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"aws.autoscaling.auto_scaling_group '{name}' already exists",
    )
    resource = ret.get("new_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert min_size == resource["min_size"]
    assert max_size == resource["max_size"]
    assert desired_capacity == resource["desired_capacity"]
    assert tags == resource["tags"]
    assert ret["old_state"] == ret["new_state"]

    # Test-run auto-scaling group update with changes
    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        test_ctx,
        name=name,
        resource_id=name,
        min_size=updated_min_size,
        max_size=updated_max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=updated_desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
        tags=updated_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == (
        f"Would update aws.autoscaling.auto_scaling_group '{name}'",
    )
    resource = ret.get("new_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert updated_min_size == resource["min_size"]
    assert updated_max_size == resource["max_size"]
    assert updated_desired_capacity == resource["desired_capacity"]
    assert updated_tags == resource["tags"]

    # Update actual auto-scaling-group-resource with changes
    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        ctx,
        name=name,
        resource_id=name,
        min_size=updated_min_size,
        max_size=updated_max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=updated_desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
        tags=updated_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated aws.autoscaling.auto_scaling_group '{name}'" in ret["comment"]
    assert f"Update tags:" in ret["comment"]
    assert (
        f"Add [[{{'ResourceType': 'auto-scaling-group', 'ResourceId': '{name}', 'Key': 'Name', 'PropagateAtLaunch': True, 'Value': 'tag-2'}}]]"
        in ret["comment"]
    )
    assert (
        f"Remove [[{{'ResourceType': 'auto-scaling-group', 'ResourceId': '{name}', 'Key': 'Name', 'PropagateAtLaunch': True, 'Value': 'tag-1'}}]]"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert updated_min_size == resource["min_size"]
    assert updated_max_size == resource["max_size"]
    assert updated_desired_capacity == resource["desired_capacity"]
    assert updated_tags == resource["tags"]

    # Describe auto-scaling group
    ret = await hub.states.aws.autoscaling.auto_scaling_group.describe(ctx)
    assert ret[name] and ret.get(name)
    resource = ret.get(name).get("aws.autoscaling.auto_scaling_group.present")
    resource_map = dict(ChainMap(*resource))

    assert name == resource_map.get("name")
    assert launch_config_name == resource_map.get("launch_configuration_name")
    assert sub_net_id == resource_map.get("vpc_zone_identifier")
    assert updated_min_size == resource_map.get("min_size")
    assert updated_max_size == resource_map.get("max_size")
    assert updated_desired_capacity == resource_map.get("desired_capacity")
    assert updated_tags == resource_map.get("tags")

    # Test-run auto-scaling group deletion
    ret = await hub.states.aws.autoscaling.auto_scaling_group.absent(
        test_ctx,
        name=name,
        resource_id=name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (
        f"Would delete aws.autoscaling.auto_scaling_group '{name}'",
    )
    resource = ret.get("old_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert updated_min_size == resource["min_size"]
    assert updated_max_size == resource["max_size"]
    assert updated_desired_capacity == resource["desired_capacity"]
    assert updated_tags == resource["tags"]

    # Test-run non-existent auto-scaling group deletion
    ret = await hub.states.aws.autoscaling.auto_scaling_group.absent(
        test_ctx,
        name=non_existent_auto_scaling_group_name,
        resource_id=non_existent_auto_scaling_group_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (
        f"aws.autoscaling.auto_scaling_group '{non_existent_auto_scaling_group_name}' already absent",
    )

    # Delete non-existent auto-scaling group
    ret = await hub.states.aws.autoscaling.auto_scaling_group.absent(
        ctx,
        name=non_existent_auto_scaling_group_name,
        resource_id=non_existent_auto_scaling_group_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (
        f"aws.autoscaling.auto_scaling_group '{non_existent_auto_scaling_group_name}' already absent",
    )
    assert not ret.get("old_state")

    # Delete auto-scaling group
    ret = await hub.states.aws.autoscaling.auto_scaling_group.absent(
        ctx, name=name, resource_id=name
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == (f"Deleted aws.autoscaling.auto_scaling_group '{name}'",)
    resource = ret.get("old_state")
    assert name == resource["name"]
    assert launch_config_name == resource["launch_configuration_name"]
    assert sub_net_id == resource["vpc_zone_identifier"]
    assert updated_min_size == resource["min_size"]
    assert updated_max_size == resource["max_size"]
    assert updated_desired_capacity == resource["desired_capacity"]
    assert updated_tags == resource["tags"]
