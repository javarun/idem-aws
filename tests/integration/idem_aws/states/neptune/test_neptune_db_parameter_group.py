import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_db_parameter_group(hub, ctx):
    # Localstack pro does not support this resource at all therefore skipping localstack tests
    if hub.tool.utils.is_running_localstack(ctx):
        return
    # Create DB Parameter Group
    db_parameter_group_name = "idem-test-neptune-db-parameter-group-" + str(
        uuid.uuid4()
    )
    tags = [
        {"Key": "Name", "Value": db_parameter_group_name},
    ]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    comment_utils_kwargs = {
        "resource_type": "aws.neptune.db_parameter_group",
        "name": db_parameter_group_name,
    }
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]
    would_update_message = hub.tool.aws.comment_utils.would_update_comment(
        **comment_utils_kwargs
    )[0]

    # --test create
    ret = await hub.states.aws.neptune.db_parameter_group.present(
        test_ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="neptune1",
        description="For testing idem plugin",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert would_create_message in ret["comment"]
    resource = ret["new_state"]
    assert db_parameter_group_name == resource["name"]
    assert "For testing idem plugin" == resource["description"]
    assert tags == resource["tags"]

    # real create
    ret = await hub.states.aws.neptune.db_parameter_group.present(
        ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="neptune1",
        description="For testing idem plugin",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert created_message in ret["comment"]
    resource = ret["new_state"]
    assert resource["name"] == db_parameter_group_name
    assert "For testing idem plugin" == resource["description"]
    assert tags == resource["tags"]
    resource_id = resource["resource_id"]

    # Describe instance
    describe_ret = await hub.states.aws.neptune.db_parameter_group.describe(ctx)
    assert resource_id in describe_ret
    resource = dict(
        ChainMap(
            *describe_ret[resource_id].get("aws.neptune.db_parameter_group.present")
        )
    )
    assert db_parameter_group_name == resource.get("name")
    assert tags == resource["tags"]
    assert "For testing idem plugin" == resource["description"]

    # Updating resource
    new_tags = [
        {"Key": "Name", "Value": f"Updated {db_parameter_group_name} during testing"},
    ]

    # --test update tags
    ret = await hub.states.aws.neptune.db_parameter_group.present(
        test_ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="neptune1",
        resource_id=resource_id,
        description="For testing idem plugin",
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert would_update_message in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags
    assert "For testing idem plugin" == ret["new_state"]["description"]

    # real update
    ret = await hub.states.aws.neptune.db_parameter_group.present(
        ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="neptune1",
        resource_id=resource_id,
        description="For testing idem plugin",
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Updated aws.neptune.db_parameter_group '{db_parameter_group_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags
    assert "For testing idem plugin" == ret["new_state"]["description"]

    # undoing update with --test as true.
    ret = await hub.states.aws.neptune.db_parameter_group.present(
        test_ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="neptune1",
        resource_id=resource_id,
        description="For testing idem plugin",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert would_update_message in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == tags
    assert "For testing idem plugin" == ret["new_state"]["description"]

    # undoing update real
    ret = await hub.states.aws.neptune.db_parameter_group.present(
        ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="neptune1",
        resource_id=resource_id,
        description="For testing idem plugin",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Updated aws.neptune.db_parameter_group '{db_parameter_group_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == tags
    assert "For testing idem plugin" == ret["new_state"]["description"]

    # Delete instance with --test as true
    ret = await hub.states.aws.neptune.db_parameter_group.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert would_delete_message in ret["comment"]
    resource = ret.get("old_state")
    assert db_parameter_group_name == resource.get("name")
    assert "For testing idem plugin" == resource["description"]

    # Delete instance
    ret = await hub.states.aws.neptune.db_parameter_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert deleted_message in ret["comment"]
    resource = ret.get("old_state")
    assert db_parameter_group_name == resource.get("name")
    assert "For testing idem plugin" == resource["description"]

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.neptune.db_parameter_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert already_absent_message in ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
