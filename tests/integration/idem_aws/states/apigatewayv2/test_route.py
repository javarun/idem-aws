import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_route(hub, ctx, aws_apigatewayv2_api):
    route_temp_name = "idem-test-route-" + str(uuid.uuid4())
    api_id = aws_apigatewayv2_api.get("api_id")
    route_key = "$connect"
    api_key_required = True
    new_api_key_required = False
    authorization_type = "NONE"
    new_authorization_type = "AWS_IAM"
    operation_name = "default"
    new_operation_name = "new_default"
    request_parameters = {"route.request.querystring.authToken": {"Required": False}}
    new_request_parameters = {"route.request.querystring.authToken": {"Required": True}}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create apigatewayv2 route with test flag
    ret = await hub.states.aws.apigatewayv2.route.present(
        test_ctx,
        name=route_temp_name,
        api_id=api_id,
        route_key=route_key,
        api_key_required=api_key_required,
        authorization_type=authorization_type,
        operation_name=operation_name,
        request_parameters=request_parameters,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigatewayv2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert api_key_required == resource.get("api_key_required")
    assert authorization_type == resource.get("authorization_type")
    assert operation_name == resource.get("operation_name")
    assert request_parameters == resource.get("request_parameters")

    # Create apigatewayv2 route
    ret = await hub.states.aws.apigatewayv2.route.present(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        route_key=route_key,
        api_key_required=api_key_required,
        authorization_type=authorization_type,
        operation_name=operation_name,
        request_parameters=request_parameters,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigatewayv2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert api_key_required == resource.get("api_key_required")
    assert authorization_type == resource.get("authorization_type")
    assert operation_name == resource.get("operation_name")
    assert request_parameters == resource.get("request_parameters")

    resource_id = resource.get("resource_id")

    # Verify that the created apigatewayv2 route is present (describe)
    ret = await hub.states.aws.apigatewayv2.route.describe(ctx)

    assert resource_id in ret
    assert "aws.apigatewayv2.route.present" in ret.get(resource_id)
    resource = ret.get(resource_id).get("aws.apigatewayv2.route.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert resource_id == resource_map.get("name")
    assert route_key == resource_map.get("route_key")
    assert api_key_required == resource_map.get("api_key_required")
    assert authorization_type == resource_map.get("authorization_type")
    assert operation_name == resource_map.get("operation_name")
    assert request_parameters == resource_map.get("request_parameters")

    # Create apigatewayv2 route again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.apigatewayv2.route.present(
        test_ctx,
        name=route_temp_name,
        api_id=api_id,
        route_key=route_key,
        resource_id=resource_id,
        api_key_required=api_key_required,
        authorization_type=authorization_type,
        operation_name=operation_name,
        request_parameters=request_parameters,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.route",
            name=route_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create apigatewayv2 route again with same resource_id and no change in state
    ret = await hub.states.aws.apigatewayv2.route.present(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        route_key=route_key,
        resource_id=resource_id,
        api_key_required=api_key_required,
        authorization_type=authorization_type,
        operation_name=operation_name,
        request_parameters=request_parameters,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.route",
            name=route_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update apigatewayv2 route with test flag
    ret = await hub.states.aws.apigatewayv2.route.present(
        test_ctx,
        name=route_temp_name,
        api_id=api_id,
        route_key=route_key,
        resource_id=resource_id,
        api_key_required=new_api_key_required,
        authorization_type=new_authorization_type,
        operation_name=new_operation_name,
        request_parameters=new_request_parameters,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.route",
            name=route_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Would update parameters: api_key_required,authorization_type,operation_name,request_parameters"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert new_api_key_required == resource.get("api_key_required")
    assert new_authorization_type == resource.get("authorization_type")
    assert new_operation_name == resource.get("operation_name")
    assert new_request_parameters == resource.get("request_parameters")

    # Update apigatewayv2 route
    ret = await hub.states.aws.apigatewayv2.route.present(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        route_key=route_key,
        resource_id=resource_id,
        api_key_required=new_api_key_required,
        authorization_type=new_authorization_type,
        operation_name=new_operation_name,
        request_parameters=new_request_parameters,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.route",
            name=route_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Updated parameters: api_key_required,authorization_type,operation_name,request_parameters"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert new_api_key_required == resource.get("api_key_required")
    assert new_authorization_type == resource.get("authorization_type")
    assert new_operation_name == resource.get("operation_name")
    assert new_request_parameters == resource.get("request_parameters")

    # Search for existing apigatewayv2 route
    ret = await hub.states.aws.apigatewayv2.route.search(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert new_api_key_required == resource.get("api_key_required")
    assert new_authorization_type == resource.get("authorization_type")
    assert new_operation_name == resource.get("operation_name")
    assert new_request_parameters == resource.get("request_parameters")

    # Search for non-existing apigatewayv2 route
    fake_resource_id = "fake-" + str(uuid.uuid4())
    ret = await hub.states.aws.apigatewayv2.route.search(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        resource_id=fake_resource_id,
    )

    assert not ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.apigatewayv2.route resource with resource id '{fake_resource_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Delete apigatewayv2 route with test flag
    ret = await hub.states.aws.apigatewayv2.route.absent(
        test_ctx,
        name=route_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigatewayv2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert new_api_key_required == resource.get("api_key_required")
    assert new_authorization_type == resource.get("authorization_type")
    assert new_operation_name == resource.get("operation_name")
    assert new_request_parameters == resource.get("request_parameters")

    # Delete apigatewayv2 route
    ret = await hub.states.aws.apigatewayv2.route.absent(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigatewayv2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert route_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert route_key == resource.get("route_key")
    assert new_api_key_required == resource.get("api_key_required")
    assert new_authorization_type == resource.get("authorization_type")
    assert new_operation_name == resource.get("operation_name")
    assert new_request_parameters == resource.get("request_parameters")

    # Delete the same apigatewayv2 route again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.apigatewayv2.route.absent(
        ctx,
        name=route_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete apigatewayv2 route with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.route.absent(
        ctx, name=route_temp_name, api_id=api_id
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
