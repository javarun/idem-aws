import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_integration(hub, ctx, aws_apigatewayv2_api):
    integration_temp_name = "idem-test-integration-" + str(uuid.uuid4())
    api_id = aws_apigatewayv2_api.get("api_id")
    integration_type = "HTTP"
    connection_type = "INTERNET"
    content_handling_strategy = "CONVERT_TO_TEXT"
    new_content_handling_strategy = "CONVERT_TO_BINARY"
    description = "Idem integration test - " + integration_temp_name
    new_description = "Idem integration test - new - " + integration_temp_name
    integration_method = "GET"
    new_integration_method = "POST"
    integration_uri = "http://idem.example.com"
    new_integration_uri = "http://new-idem.example.com"
    passthrough_behavior = "WHEN_NO_MATCH"
    new_passthrough_behavior = "NEVER"
    payload_format_version = "1.0"
    request_parameters = {
        "integration.request.header.authToken": "route.request.querystring.authToken"
    }
    new_request_parameters = {
        "integration.request.header.authToken": "route.request.querystring.authToken"
    }
    request_templates = {"application/json": '{"statusCode":200}'}
    new_request_templates = {"application/json": '{"statusCode":201}'}
    template_selection_expression = "expression"
    new_template_selection_expression = "new_expression"
    timeout_in_millis = 60
    new_timeout_in_millis = 120

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create apigatewayv2 integration with test flag
    ret = await hub.states.aws.apigatewayv2.integration.present(
        test_ctx,
        name=integration_temp_name,
        api_id=api_id,
        integration_type=integration_type,
        connection_type=connection_type,
        content_handling_strategy=content_handling_strategy,
        description=description,
        integration_method=integration_method,
        integration_uri=integration_uri,
        passthrough_behavior=passthrough_behavior,
        payload_format_version=payload_format_version,
        request_parameters=request_parameters,
        request_templates=request_templates,
        template_selection_expression=template_selection_expression,
        timeout_in_millis=timeout_in_millis,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigatewayv2.integration", name=integration_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert content_handling_strategy == resource.get("content_handling_strategy")
    assert description == resource.get("description")
    assert integration_method == resource.get("integration_method")
    assert integration_uri == resource.get("integration_uri")
    assert passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert request_parameters == resource.get("request_parameters")
    assert request_templates == resource.get("request_templates")
    assert template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert timeout_in_millis == resource.get("timeout_in_millis")

    # Create apigatewayv2 integration
    ret = await hub.states.aws.apigatewayv2.integration.present(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        integration_type=integration_type,
        connection_type=connection_type,
        content_handling_strategy=content_handling_strategy,
        description=description,
        integration_method=integration_method,
        integration_uri=integration_uri,
        passthrough_behavior=passthrough_behavior,
        payload_format_version=payload_format_version,
        request_parameters=request_parameters,
        request_templates=request_templates,
        template_selection_expression=template_selection_expression,
        timeout_in_millis=timeout_in_millis,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigatewayv2.integration", name=integration_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert content_handling_strategy == resource.get("content_handling_strategy")
    assert description == resource.get("description")
    assert integration_method == resource.get("integration_method")
    assert integration_uri == resource.get("integration_uri")
    assert passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert request_parameters == resource.get("request_parameters")
    assert request_templates == resource.get("request_templates")
    assert template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert timeout_in_millis == resource.get("timeout_in_millis")

    resource_id = resource.get("resource_id")

    # Verify that the created apigatewayv2 integration is present (describe)
    ret = await hub.states.aws.apigatewayv2.integration.describe(ctx)

    assert resource_id in ret
    assert "aws.apigatewayv2.integration.present" in ret.get(resource_id)
    resource = ret.get(resource_id).get("aws.apigatewayv2.integration.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert resource_id == resource_map.get("name")
    assert integration_type == resource_map.get("integration_type")
    assert connection_type == resource_map.get("connection_type")
    assert content_handling_strategy == resource_map.get("content_handling_strategy")
    assert description == resource_map.get("description")
    assert integration_method == resource_map.get("integration_method")
    assert integration_uri == resource_map.get("integration_uri")
    assert passthrough_behavior == resource_map.get("passthrough_behavior")
    assert payload_format_version == resource_map.get("payload_format_version")
    assert request_parameters == resource_map.get("request_parameters")
    assert request_templates == resource_map.get("request_templates")
    assert template_selection_expression == resource_map.get(
        "template_selection_expression"
    )
    assert timeout_in_millis == resource_map.get("timeout_in_millis")

    # Create apigatewayv2 integration again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.apigatewayv2.integration.present(
        test_ctx,
        name=integration_temp_name,
        api_id=api_id,
        integration_type=integration_type,
        resource_id=resource_id,
        connection_type=connection_type,
        content_handling_strategy=content_handling_strategy,
        description=description,
        integration_method=integration_method,
        integration_uri=integration_uri,
        passthrough_behavior=passthrough_behavior,
        payload_format_version=payload_format_version,
        request_parameters=request_parameters,
        request_templates=request_templates,
        template_selection_expression=template_selection_expression,
        timeout_in_millis=timeout_in_millis,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.integration",
            name=integration_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create apigatewayv2 integration again with same resource_id and no change in state
    ret = await hub.states.aws.apigatewayv2.integration.present(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        integration_type=integration_type,
        resource_id=resource_id,
        connection_type=connection_type,
        content_handling_strategy=content_handling_strategy,
        description=description,
        integration_method=integration_method,
        integration_uri=integration_uri,
        passthrough_behavior=passthrough_behavior,
        payload_format_version=payload_format_version,
        request_parameters=request_parameters,
        request_templates=request_templates,
        template_selection_expression=template_selection_expression,
        timeout_in_millis=timeout_in_millis,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.integration",
            name=integration_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update apigatewayv2 integration with test flag
    ret = await hub.states.aws.apigatewayv2.integration.present(
        test_ctx,
        name=integration_temp_name,
        api_id=api_id,
        integration_type=integration_type,
        resource_id=resource_id,
        connection_type=connection_type,
        content_handling_strategy=new_content_handling_strategy,
        description=new_description,
        integration_method=new_integration_method,
        integration_uri=new_integration_uri,
        passthrough_behavior=new_passthrough_behavior,
        payload_format_version=payload_format_version,
        request_parameters=new_request_parameters,
        request_templates=new_request_templates,
        template_selection_expression=new_template_selection_expression,
        timeout_in_millis=new_timeout_in_millis,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.integration",
            name=integration_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Would update parameters: content_handling_strategy,description,integration_method,integration_uri,passthrough_behavior,request_templates,template_selection_expression,timeout_in_millis"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert new_content_handling_strategy == resource.get("content_handling_strategy")
    assert new_description == resource.get("description")
    assert new_integration_method == resource.get("integration_method")
    assert new_integration_uri == resource.get("integration_uri")
    assert new_passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert new_request_parameters == resource.get("request_parameters")
    assert new_request_templates == resource.get("request_templates")
    assert new_template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert new_timeout_in_millis == resource.get("timeout_in_millis")

    # Update apigatewayv2 integration
    ret = await hub.states.aws.apigatewayv2.integration.present(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        integration_type=integration_type,
        resource_id=resource_id,
        connection_type=connection_type,
        content_handling_strategy=new_content_handling_strategy,
        description=new_description,
        integration_method=new_integration_method,
        integration_uri=new_integration_uri,
        passthrough_behavior=new_passthrough_behavior,
        payload_format_version=payload_format_version,
        request_parameters=new_request_parameters,
        request_templates=new_request_templates,
        template_selection_expression=new_template_selection_expression,
        timeout_in_millis=new_timeout_in_millis,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.integration",
            name=integration_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Updated parameters: content_handling_strategy,description,integration_method,integration_uri,passthrough_behavior,request_templates,template_selection_expression,timeout_in_millis"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert new_content_handling_strategy == resource.get("content_handling_strategy")
    assert new_description == resource.get("description")
    assert new_integration_method == resource.get("integration_method")
    assert new_integration_uri == resource.get("integration_uri")
    assert new_passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert new_request_parameters == resource.get("request_parameters")
    assert new_request_templates == resource.get("request_templates")
    assert new_template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert new_timeout_in_millis == resource.get("timeout_in_millis")

    # Search for existing apigatewayv2 integration
    ret = await hub.states.aws.apigatewayv2.integration.search(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert new_content_handling_strategy == resource.get("content_handling_strategy")
    assert new_description == resource.get("description")
    assert new_integration_method == resource.get("integration_method")
    assert new_integration_uri == resource.get("integration_uri")
    assert new_passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert new_request_parameters == resource.get("request_parameters")
    assert new_request_templates == resource.get("request_templates")
    assert new_template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert new_timeout_in_millis == resource.get("timeout_in_millis")

    # Search for non-existing apigatewayv2 integration
    fake_resource_id = "fake-" + str(uuid.uuid4())
    ret = await hub.states.aws.apigatewayv2.integration.search(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        resource_id=fake_resource_id,
    )

    assert not ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.apigatewayv2.integration resource with resource id '{fake_resource_id}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")

    # Delete apigatewayv2 integration with test flag
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        test_ctx,
        name=integration_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigatewayv2.integration", name=integration_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert new_content_handling_strategy == resource.get("content_handling_strategy")
    assert new_description == resource.get("description")
    assert new_integration_method == resource.get("integration_method")
    assert new_integration_uri == resource.get("integration_uri")
    assert new_passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert new_request_parameters == resource.get("request_parameters")
    assert new_request_templates == resource.get("request_templates")
    assert new_template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert new_timeout_in_millis == resource.get("timeout_in_millis")

    # Delete apigatewayv2 integration
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigatewayv2.integration", name=integration_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert integration_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert integration_type == resource.get("integration_type")
    assert connection_type == resource.get("connection_type")
    assert new_content_handling_strategy == resource.get("content_handling_strategy")
    assert new_description == resource.get("description")
    assert new_integration_method == resource.get("integration_method")
    assert new_integration_uri == resource.get("integration_uri")
    assert new_passthrough_behavior == resource.get("passthrough_behavior")
    assert payload_format_version == resource.get("payload_format_version")
    assert new_request_parameters == resource.get("request_parameters")
    assert new_request_templates == resource.get("request_templates")
    assert new_template_selection_expression == resource.get(
        "template_selection_expression"
    )
    assert new_timeout_in_millis == resource.get("timeout_in_millis")

    # Delete the same apigatewayv2 integration again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        ctx,
        name=integration_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.integration", name=integration_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete apigatewayv2 integration with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.integration.absent(
        ctx, name=integration_temp_name, api_id=api_id
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.integration", name=integration_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
