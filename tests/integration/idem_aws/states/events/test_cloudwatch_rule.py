import copy
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_cloudwatch_rule(
    hub, ctx, aws_lambda_function, aws_sns_topic, aws_iam_role_2
):
    # Create CloudWatchEvents rule

    rule_name = "idem-test-events-rule" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": rule_name}]
    description = "Create CloudWatchEvents Rule."
    state = "ENABLED"
    schedule_expression = "rate(12 hours)"
    event_bus_name = "default"
    event_pattern = (
        '{\n  "source": ["aws.lambda"],\n  "detail-type": ["AWS API Call via CloudTrail"],\n  '
        '"detail": {\n    "eventSource": ["lambda.amazonaws.com"]\n  }\n}'
    )
    lambda_arn_id = str(uuid.uuid4())
    sns_topic = str(uuid.uuid4())
    targets = [
        {"Id": lambda_arn_id, "Arn": aws_lambda_function.get("function_arn")},
        {"Id": sns_topic, "Arn": aws_sns_topic.get("resource_id")},
    ]
    role_arn = aws_iam_role_2["arn"]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Idem state --test
    ret = await hub.states.aws.events.rule.present(
        ctx=test_ctx,
        name=rule_name,
        tags=tags,
        rule_status=state,
        targets=targets,
        schedule_expression=schedule_expression,
        description=description,
        event_pattern=event_pattern,
        event_bus_name=event_bus_name,
        role_arn=role_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Would create aws.events.rule '{rule_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert rule_name == resource.get("name")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert resource.get("tags")
    assert resource.get("tags")[0].get("Key") == tags[0].get("Key")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")

    # Create CloudWatchEvents rule.
    ret = await hub.states.aws.events.rule.present(
        ctx=ctx,
        name=rule_name,
        tags=tags,
        rule_status=state,
        targets=targets,
        schedule_expression=schedule_expression,
        description=description,
        event_pattern=event_pattern,
        event_bus_name=event_bus_name,
        role_arn=role_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.events.rule '{rule_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert rule_name == resource.get("name")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert resource.get("tags")
    assert resource.get("tags")[0].get("Key") == tags[0].get("Key")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")
    resource_id = ret.get("name")
    rule_arn = resource.get("arn")

    tags = [
        {"Key": "Name", "Value": rule_name},
        {"Key": "new_tag", "Value": "new_value"},
    ]
    description = "Update CloudWatchEvents Rule."
    state = "ENABLED"
    schedule_expression = "rate(12 hours)"
    event_bus_name = "default"
    targets = [
        {"Id": sns_topic, "Arn": aws_lambda_function.get("function_arn")},
        {"Id": lambda_arn_id, "Arn": aws_sns_topic.get("resource_id")},
    ]

    # Idem state --test. Update CloudWatchEvents Rule
    ret = await hub.states.aws.events.rule.present(
        ctx=test_ctx,
        name=rule_name,
        tags=tags,
        rule_status=state,
        targets=targets,
        schedule_expression=schedule_expression,
        description=description,
        event_pattern=event_pattern,
        event_bus_name=event_bus_name,
        role_arn=role_arn,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert rule_name == resource.get("name")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")
    assert (resource.get("tags") and len(resource.get("targets")) == 2) and all(
        tag in resource.get("tags") for tag in tags
    )

    # Update CloudWatchEvents Rule
    ret = await hub.states.aws.events.rule.present(
        ctx=ctx,
        name=rule_name,
        tags=tags,
        targets=targets,
        rule_status=state,
        schedule_expression=schedule_expression,
        description=description,
        event_pattern=event_pattern,
        event_bus_name=event_bus_name,
        role_arn=role_arn,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert rule_name == resource.get("name")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")
    assert (resource.get("tags") and len(resource.get("targets")) == 2) and all(
        tag in resource.get("tags") for tag in tags
    )

    # Idem state --test. Update CloudWatchEvents Rule. Just update two params. Do not pass other arguments.
    state = "DISABLED"
    schedule_expression = "rate(24 hours)"
    ret = await hub.states.aws.events.rule.present(
        ctx=test_ctx,
        name=rule_name,
        rule_status=state,
        schedule_expression=schedule_expression,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")
    assert (resource.get("tags") and len(resource.get("targets")) == 2) and all(
        tag in resource.get("tags") for tag in tags
    )
    assert rule_name == resource.get("name")

    # Update CloudWatchEvents Rule. Just update two params. Do not pass other arguments.
    state = "DISABLED"
    schedule_expression = "rate(24 hours)"
    ret = await hub.states.aws.events.rule.present(
        ctx=ctx,
        name=rule_name,
        rule_status=state,
        schedule_expression=schedule_expression,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")
    assert (resource.get("tags") and len(resource.get("targets")) == 2) and all(
        tag in resource.get("tags") for tag in tags
    )
    assert rule_name == resource.get("name")

    # Idem state --test. Update CloudWatchEvents Rule. Pass NULL target list. Targets should be intact.
    ret = await hub.states.aws.events.rule.present(
        ctx=test_ctx,
        name=rule_name,
        rule_status=state,
        targets=None,
        schedule_expression=schedule_expression,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )

    # Update CloudWatchEvents Rule. Pass NULL target list. Targets should be intact.
    ret = await hub.states.aws.events.rule.present(
        ctx=ctx,
        name=rule_name,
        rule_status=state,
        targets=None,
        schedule_expression=schedule_expression,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )

    # Describe CloudWatchEvents Rule
    describe_ret = await hub.states.aws.events.rule.describe(ctx)
    assert describe_ret[rule_arn]
    resource = describe_ret[rule_arn]["aws.events.rule.present"][0]
    assert resource.get("tags")
    assert (len(resource.get("tags")) == 2) and all(
        tag in resource.get("tags") for tag in tags
    )
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")

    # Idem state --test. Delete CloudWatchEvents Rule
    ret = await hub.states.aws.events.rule.absent(
        test_ctx,
        name=rule_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.events.rule '{rule_name}'" in ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))

    # Delete CloudWatchEvents Rule
    ret = await hub.states.aws.events.rule.absent(
        ctx,
        name=rule_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))
    resource = ret.get("old_state")
    assert schedule_expression == resource.get("schedule_expression")
    assert state == resource.get("rule_status")
    assert (len(resource.get("targets")) == 2) and all(
        target in resource.get("targets") for target in targets
    )
    assert event_bus_name == resource.get("event_bus_name")
    assert role_arn == resource.get("role_arn")
    assert event_pattern == resource.get("event_pattern")
    assert description == resource.get("description")

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.events.rule.absent(
        ctx,
        name=rule_name,
        resource_id=resource_id,
    )
    assert f"aws.events.rule '{rule_name}' already absent" in ret["comment"]
