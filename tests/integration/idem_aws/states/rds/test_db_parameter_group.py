import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_db_parameter_group(hub, ctx):
    # To skip running on localstack as localstack/pro is not able to support modifying the resource, test is working with real aws
    # Localstack does not support, so skipping it
    if hub.tool.utils.is_running_localstack(ctx):
        return
    db_parameter_group_name = "db-parameter-group-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": db_parameter_group_name}]
    parameters = [
        {"name": "aurora_disable_hash_join", "value": "1", "apply_method": "immediate"}
    ]

    # Create db_parameter_group with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.rds.db_parameter_group.present(
        test_ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="aurora-mysql5.7",
        description=db_parameter_group_name,
        tags=tags,
        parameters=parameters,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.rds.db_parameter_group '{db_parameter_group_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert db_parameter_group_name == resource.get("name")

    # Create db_parameter_group
    ret = await hub.states.aws.rds.db_parameter_group.present(
        ctx,
        name=db_parameter_group_name,
        db_parameter_group_family="aurora-mysql5.7",
        description=db_parameter_group_name,
        tags=tags,
        parameters=parameters,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.rds.db_parameter_group '{db_parameter_group_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert db_parameter_group_name == resource.get("name")
    resource_id = resource.get("resource_id")

    # Create describe db_parameter_group
    describe_ret = await hub.states.aws.rds.db_parameter_group.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.rds.db_parameter_group.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.rds.db_parameter_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert tags == described_resource_map.get("tags")
    tags.append(
        {
            "Key": f"idem-test-vpc-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-vpc-value-{str(uuid.uuid4())}",
        }
    )
    # Update db_parameter_group with test flag
    ret = await hub.states.aws.rds.db_parameter_group.present(
        test_ctx,
        name=db_parameter_group_name,
        resource_id=resource_id,
        db_parameter_group_family="aurora-mysql5.7",
        description=db_parameter_group_name,
        tags=tags,
        parameters=parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    # Update db_parameter_group real
    ret = await hub.states.aws.rds.db_parameter_group.present(
        ctx,
        name=db_parameter_group_name,
        resource_id=resource_id,
        db_parameter_group_family="aurora-mysql5.7",
        description=db_parameter_group_name,
        tags=tags,
        parameters=parameters,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    real_db_parameter_group_tags = resource.get("tags")
    assert real_db_parameter_group_tags
    assert 2 == len(real_db_parameter_group_tags)
    assert tags[0] in real_db_parameter_group_tags
    assert tags[1] in real_db_parameter_group_tags
    # Delete db_parameter_group with test flag
    ret = await hub.states.aws.rds.db_parameter_group.absent(
        test_ctx, name=db_parameter_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.rds.db_parameter_group '{db_parameter_group_name}'"
        in ret["comment"]
    )
    # Delete db_parameter_group
    ret = await hub.states.aws.rds.db_parameter_group.absent(
        ctx, name=db_parameter_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    # Delete db_parameter_group again
    ret = await hub.states.aws.rds.db_parameter_group.absent(
        ctx, name=db_parameter_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.rds.db_parameter_group '{db_parameter_group_name}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_db_parameter_group_absent_with_none_resource_id(hub, ctx):
    db_parameter_group_name = "db-parameter-group-" + str(uuid.uuid4())
    # Delete db_parameter_group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.rds.db_parameter_group.absent(
        ctx, name=db_parameter_group_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.rds.db_parameter_group '{db_parameter_group_name}' already absent"
        in ret["comment"]
    )
