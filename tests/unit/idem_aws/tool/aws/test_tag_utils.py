def test_diff_tags_dict_update(hub, mock_hub):
    old_tags = {"Name": "resource-name", "tag1": "tag1-value"}
    new_tags = {"Name": "resource-name-new", "tag1": "tag1-value"}
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == {"Name": "resource-name-new"}
    assert tags_to_remove == {"Name": "resource-name"}


def test_diff_tags_dict_no_old_tag(hub, mock_hub):
    old_tags = {}
    new_tags = {"Name": "resource-name", "tag1": "tag1-value"}
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == {"Name": "resource-name", "tag1": "tag1-value"}
    assert tags_to_remove == {}


def test_diff_tags_dict_with_old_tag_none(hub, mock_hub):
    """Test when old_tag is None, old_tag is default to empty."""
    old_tags = None
    new_tags = {"Name": "resource-name", "tag1": "tag1-value"}
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == {"Name": "resource-name", "tag1": "tag1-value"}
    assert tags_to_remove == {}


def test_diff_tags_dict_no_new_tag(hub, mock_hub):
    old_tags = {"Name": "resource-name", "tag1": "tag1-value"}
    new_tags = {}
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == {}
    assert tags_to_remove == {"Name": "resource-name", "tag1": "tag1-value"}


def test_diff_tags_dict_new_tag_none(hub, mock_hub):
    """Test when new_tag is None, new_tag is default to empty."""
    old_tags = {"Name": "resource-name", "tag1": "tag1-value"}
    new_tags = None
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == {}
    assert tags_to_remove == {"Name": "resource-name", "tag1": "tag1-value"}


def test_diff_tags_list_update(hub, mock_hub):
    old_tags = [
        {"Key": "Name", "Value": "resource-name"},
        {"Key": "tag1", "Value": "tag1-value"},
    ]
    new_tags = [
        {"Key": "Name", "Value": "resource-name-new"},
        {"Key": "tag1", "Value": "tag1-value"},
    ]
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_list(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == [{"Key": "Name", "Value": "resource-name-new"}]
    assert tags_to_remove == [{"Key": "Name", "Value": "resource-name"}]


def test_diff_tags_list_update_no_new_tag(hub, mock_hub):
    old_tags = [
        {"Key": "Name", "Value": "resource-name"},
        {"Key": "tag1", "Value": "tag1-value"},
    ]
    new_tags = []
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_list(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == []
    assert tags_to_remove == [
        {"Key": "Name", "Value": "resource-name"},
        {"Key": "tag1", "Value": "tag1-value"},
    ]


def test_diff_tags_list_update_no_old_tag(hub, mock_hub):
    old_tags = []
    new_tags = [
        {"Key": "Name", "Value": "resource-name"},
        {"Key": "tag1", "Value": "tag1-value"},
    ]
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_list(
        old_tags=old_tags, new_tags=new_tags
    )
    assert tags_to_add == [
        {"Key": "Name", "Value": "resource-name"},
        {"Key": "tag1", "Value": "tag1-value"},
    ]
    assert tags_to_remove == []
